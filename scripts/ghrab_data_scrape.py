# import libraries
import urllib.request as urllib
from bs4 import BeautifulSoup
import csv
from datetime import datetime

#specify the url
quote_page = 'https://www.georgiaarchives.org/ghrac/print_view/?rec=400'
#TODO: loop over pages

# query website and return html to variable 'page'
page = urllib.urlopen(quote_page)

# parse html w/ BeautifulSoup and store in 'soup'
soup = BeautifulSoup(page, 'html.parser')

#find the instance of the h2 styled this way 
name_box = soup.find('h2', attrs={'style': 'margin-bottom: 0em;'})
# strip out the starting and trailing tags
name = name_box.text.strip()
print(name)

# grab the table
table_data = soup.find('table')
#for table_strip in table_data.children:
print(table_data.get_text("|", strip=True))

table_strip = table_data.text.strip()
#print(table_strip)

#open a csv file with append, so old data won't be erased
#with open('ghrab.csv', 'a') as csv_file:
#    writer = csv.writer(csv_file)
#    writer.writerow(name, table_data, datetime.now())